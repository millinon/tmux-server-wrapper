#!/bin/bash

SRC=$(readlink "${BASH_SOURCE[0]}")
DIR=$(dirname "$SRC")
SES=$(cat "$DIR/session.tmux")

shopt -s expand_aliases
alias tmux='tmux -S $DIR/tmux.socket'

attach () {
	unset TMUX
	echo "Press Ctrl+B, D to disconnect"
	read -p "Press ENTER to continue..."
	tmux attach -t "$SES"
}

if tmux has-session -t "$SES" 2>/dev/null; then
	if [ -n "$TMUX" ] && [ "$SES" = "$(tmux list-sessions | sed -n '/(attached)/s/:.*//p')" ] ; then
		echo "Session $SES already attached, nesting is bad"
		read -p "Attach anyway? (yes/no) " resp
		case $resp in
			[Yy]*) 
				attach ;; 
		esac
	else
		attach
	fi	
	exit 0
else
	echo "Error: session ${SES} not running"
	echo "Run start.sh to start the session"
	exit 1
fi
