#!/bin/bash

SRC=$(readlink "${BASH_SOURCE[0]}")
DIR=$(dirname "$SRC")
SES=$(cat "$DIR/session.tmux")
#unset TMUX

shopt -s expand_aliases
alias tmux='tmux -S $DIR/tmux.socket'

if tmux has-session -t "$SES" 2>/dev/null; then
	echo "Killing session ${SES}..."

	# Optionally try to gracefully stop the process
#	tmux send-keys -t "$SES" "stop Enter"
#	sleep 5
	tmux kill-session -t "$SES"
	echo "Session killed"
else
	echo "Error: session ${SES} not running"
fi
