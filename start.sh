#!/bin/bash

SRC=$(readlink "${BASH_SOURCE[0]}")
DIR=$(dirname "$SRC")
SES=$(cat "$DIR/session.tmux")

shopt -s expand_aliases
alias tmux="tmux -S \"$DIR/tmux.socket\""

if tmux has-session -t "$SES" 2>/dev/null; then
	echo "Session ${SES} already running"
    exit 1
else
	echo "Starting session ${SES}..."
	tmux new -s "$SES" -d "$DIR/server.sh"
	echo "Session started"
fi
